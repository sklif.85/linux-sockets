Programming Sockets in Linux

- Socket API Basics
- Socket attributes
- Addresses
- Server connection setup
- Client connection setup
- Data exchange
- Datagrams exchange
- Using low-level sockets
- Parallel customer service
- Closing the socket
- Error processing

Programming language - C