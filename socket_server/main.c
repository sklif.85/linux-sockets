/**
 * SERVER_SOCKET
 * Created by alexwestside on 09.02.18.
 */

#include <stdlib.h>
#include <memory.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>

void handle_error(char *error, int error_id)
{
    perror(error);
    exit(error_id);
}

int main(void)
{
    char    *messsage;
    char    *buff;
    int     socket_fd;
    int     listener;
    struct  sockaddr_in addr;
    ssize_t bytes_read;
    size_t  buff_size;
    char    *client_message;

    messsage = "Yeahhh, bro!!!!";

    buff_size = 1024;
    buff = (char *)malloc(sizeof(char) * buff_size);
    if (!buff)
        handle_error("malloc", 1);
    bzero(buff, sizeof(char) * buff_size);

    client_message = (char *)malloc(sizeof(char) * buff_size);
    if (!client_message)
        handle_error("malloc", 1);
    bzero(client_message, sizeof(char) * buff_size);

    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener < 0)
        handle_error("listener", 2);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(8888);
    addr.sin_addr.s_addr = htonl(INADDR_ANY);

    if (bind(listener, (const struct sockaddr *)&addr, sizeof(addr)) < 0)
        handle_error("bind", 3);

    listen(listener, 1);

    while (1)
    {
        socket_fd = accept(listener, NULL, NULL);
        if (socket_fd < 0)
            handle_error("accept", 4);

        while (1)
        {
            bytes_read = recv(socket_fd, buff, buff_size, 0);
            if (bytes_read <= 0)
                break;
            printf("Server readed %d bytes\n\n", bytes_read);
            memcpy(client_message, buff, bytes_read);
            printf("Receive message from client -> %s\n\n", client_message);
            send(socket_fd, messsage, (size_t)bytes_read, 0);
        }
        close(socket_fd);
        close(listener);
    }
    return 0;
}
