/**
 * CLIENT_SOCKET
 * Created by alexwestside on 09.02.18.
 */

#include <stdlib.h>
#include <memory.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <unistd.h>

void handle_error(char *error, int error_id)
{
    perror(error);
    exit(error_id);
}

int send_all(int socket_fd, char *buf, size_t len, int flags)
{
    int     total;
    ssize_t n;

    total = 0;
    n = 0;
    while (total < len)
    {
        if ((n = send(socket_fd, buf + total, len - total, flags)) == -1)
            break ;
        total += n;
    }
    return (n == -1 ? -1 : total);
}

int recv_all(int socket_fd, char **buf, size_t len, size_t type, int flags, char **server_message)
{
    char    *p;
    size_t  n;

    *server_message = (char *)malloc(sizeof(char) * len);
    p = *server_message;
    while (len)
    {
        if ((n = (size_t)recv(socket_fd, *buf, type, flags)) == 0 )
            break ;
        len -= n;
        memcpy(p, *buf, n);
        p += n;
    }
    return (n == len ? 1 : 0);
}

int main(void)
{
    char    *messsage;
    char    *buff;
    int     socket_fd;
    struct  sockaddr_in addr;
    char    *server_message;

    messsage = "Socket, mothefacker...do you use it???";
    printf("Send message to server -> %s\n\n", messsage);

    buff = (char *)malloc(sizeof(char) * strlen(messsage));
    if (!buff)
        handle_error("malloc", 1);

    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
        handle_error("socket_fd", 2);

    addr.sin_family = AF_INET;
    addr.sin_port = htons(8888);
    addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

    if (connect(socket_fd, (const struct sockaddr *)&addr, sizeof(addr)) < 0)
        handle_error("connect", 3);

    if (send_all(socket_fd, messsage, strlen(messsage), 0) < 0)
        handle_error("send", 4);

    if (recv_all(socket_fd, &buff, strlen(messsage), sizeof(messsage), 0, &server_message) == 0)
    {
        printf("Receive message from server -> %s\n\n", server_message);
        close(socket_fd);
        handle_error("receive", 5);
    }
    return 0;
}

